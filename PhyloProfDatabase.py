from os import path
import os
import sys

# Copyright (c) 2008-2012, Daniel S. Standage
# 
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

class PhyloProfDatabase(object):
  """
  A class for generating and working with PhyloProf databases: protein or
  nucleotide Fasta files which have been indexed for BLAST searches and for
  PhyloProf
  """
  __dbpath = ""
  __dbtype = "prot"
  __blastindexpath = ""
  __ppindexpath = ""
  __ppindex = -1
  
  def __init__(self, dbpath, dbtype="prot", autoindex=True):
    """
    Constructor: store database path and type, do some sanity checks, and maybe
    auto-index the database (check if indexes exist and create them if not)
    """
    self.__dbpath = dbpath
    self.__dbtype = dbtype
    
    if not path.exists(self.__dbpath):
      print >> sys.stderr, "[PhyloProfDatabase] error: database '%s' does not exist" % self.__dbpath
    if self.__dbtype != "prot" and self.__dbtype != "nucl":
      print >> sys.stderr, "[PhyloProfDatabase] error: unsupported database type '%s'" % self.__dbtype
    
    char = "p"
    if self.__dbtype == "nucl":
      char = "n"
    self.__blastindexpath = "%s.%cin" % (self.__dbpath, char)
    self.__ppindexpath = "%s.%cppindex" % (self.__dbpath, char)
    
    if autoindex:
      self.createBlastIndex(False)
      self.createPhyloProfIndex(False)


  def createBlastIndex(self, forceReIndex = True):
    """Create the BLAST index for the database"""
    if not path.exists(self.__blastindexpath) or forceReIndex:
      command = "makeblastdb -in %s -dbtype %s -parse_seqids" % (self.__dbpath, self.__dbtype)
      print >> sys.stderr, "[PhyloProfDatabase] generating BLAST index: '%s'" % command
      os.system(command + " | grep -v '^$' 1>&2")


  def createPhyloProfIndex(self, forceReIndex = True):
    """Create the PhyloProf index for the database"""
    if not path.exists(self.__ppindexpath) or forceReIndex:
      prog = "./ppindex"
      if not path.exists(prog):
        prog = "ppindex"
      command = "%s < %s > %s" % (prog, self.__dbpath, self.__ppindexpath)
      print >> sys.stderr, "[PhyloProfDatabase] generating PhyloProf index: '%s'" % command
      os.system(command)


  def getPhyloProfIndex(self):
    """Parse the PhyloProf index and return as a dictionary"""
    if self.__ppindex == -1:
      self.__ppindex = {}
      ppindex = open(self.__ppindexpath, "r")
      for line in ppindex:
        values = []
        for token in line.split():
          values.append(token)
        values[0] = int(values[0])
        self.__ppindex[values[1]] = values[0]
        
    return self.__ppindex
