#!/usr/bin/python

# Copyright (c) 2008-2012, Daniel S. Standage
# 
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
# 
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

from os import path
import os
from optparse import OptionParser
import sys
from PhyloProfDatabase import PhyloProfDatabase

#
blast_prog_to_dbtype = {"blastn":"nucl", "tblastn":"nucl", "tblastx":"nucl", "blastp":"prot", "blastx":"prot"}

# Specify command-line interface
usage = "Usage: %prog [options] sequences.fasta database.fasta"
parser = OptionParser(usage=usage)
parser.add_option("-d", "--mindens", dest="m", type="float",  default=0.05,     help="minimum density threshold (as a percentage)")
parser.add_option("-e", "--evalue",     dest="e", type="string", default="1e-10",  help="expect value cutoff for BLAST searches")
parser.add_option("-p", "--program",    dest="p", type="string", default="blastp", help="BLAST program to use")

# Parse and validate options
(options, args) = parser.parse_args()
if(len(args) != 2):
  parser.error("must provide sequence file and database file")
(seqfile, dbfile) = args
if(options.p not in blast_prog_to_dbtype):
  parser.error("unsupported BLAST program '%s'" % options.p)
dbtype = blast_prog_to_dbtype[ options.p ]

#
db = PhyloProfDatabase(dbfile, dbtype)
ppindex = db.getPhyloProfIndex()

#
blastresultsfile = "phyloprof-blastresults-%s.temp" % os.path.basename(seqfile)
blastlogfile = "phyloprof-blastlog-%s.temp" % os.path.basename(seqfile)
blastcommand = "%s -query %s -db %s -out %s -evalue %s -outfmt 10 2> %s" % (options.p, seqfile, dbfile, blastresultsfile, options.e, blastlogfile)
print >> sys.stderr, "[PhyloProf] running BLAST: '%s'" % blastcommand
os.system(blastcommand)

#
profiles = {}
blastresults = open(blastresultsfile, "r")
for line in blastresults:
  fields = line.split(",")
  queryid = fields[0]
  match = fields[1]
  matchfields = fields[1].split("|")
  matchorganism = matchfields[1]
  if queryid not in profiles:
    profiles[ queryid ] = ["0" for i in xrange(len(ppindex))]
  profiles[ queryid ][ ppindex[matchorganism] ] = "1"

#
for queryid in profiles:
  profile = "".join( profiles[queryid] )
  density = float(profile.count("1")) / float(len(profile))
  if(density >= options.m):
    print ">%s\n%s" % (queryid, profile)

os.remove(blastresultsfile)
os.remove(blastlogfile)

print >> sys.stderr, "[PhyloProf] done!"
